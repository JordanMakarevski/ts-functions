let integersArray: Array<number> = [1, 2, 54, 3, 4 , 10, 15, 23, 64, 532];

function largerThanTen (arrayIntegers : Array<number>) : Array<number>{
    let moreThanTen: Array<number> = arrayIntegers.filter(function(element){
        return element > 10;
    });
    return moreThanTen;
};
console.log(largerThanTen(integersArray));

let stringsArray: string[] = ["john", "mug", "cat", "dog", "teLeVizOr"];

function makeUpperCase (strings : string[]): string[]{
    let upperCase : string[] = strings.map(function(element){
        return element.toUpperCase();
    });
    return upperCase;
}
console.log(makeUpperCase(stringsArray));

let denars = 12300;

const currencyExchanger = (denars : number) : number => { return denars/61.5 };

console.log(currencyExchanger(denars));

const url = " https://jsonplaceholder.typicode.com/todos/";

async function getTitles(url:string){
    const promise : Promise<Response> = fetch(url);
    try{
        const response = await promise;
        const data = await response.json();
        const completed = data.filter(function(element){
            return element.completed;
          })
          console.log(completed.map(function(element){
              return element.title;
          }))
          
    }
    catch(error){
        console.log(error);
    }
}
getTitles(url);