var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
let integersArray = [1, 2, 54, 3, 4, 10, 15, 23, 64, 532];
function largerThanTen(arrayIntegers) {
    let moreThanTen = arrayIntegers.filter(function (element) {
        return element > 10;
    });
    return moreThanTen;
}
;
console.log(largerThanTen(integersArray));
let stringsArray = ["john", "mug", "cat", "dog", "teLeVizOr"];
function makeUpperCase(strings) {
    let upperCase = strings.map(function (element) {
        return element.toUpperCase();
    });
    return upperCase;
}
console.log(makeUpperCase(stringsArray));
let denars = 12300;
const currencyExchanger = (denars) => { return denars / 61.5; };
console.log(currencyExchanger(denars));
const url = " https://jsonplaceholder.typicode.com/todos/";
function getTitles(url) {
    return __awaiter(this, void 0, void 0, function* () {
        const promise = fetch(url);
        try {
            const response = yield promise;
            const data = yield response.json();
            const completed = data.filter(function (element) {
                return element.completed;
            });
            console.log(completed.map(function (element) {
                return element.title;
            }));
        }
        catch (error) {
            console.log(error);
        }
    });
}
getTitles(url);
